package com.iukcy.solrdemo;

import com.alibaba.fastjson.JSONObject;
import com.iukcy.solrdemo.model.TextDataDO;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @project: solr-workspace
 * @author: zhangzhufu
 * @datetime: 2021/1/25 下午12:16
 * @description: 测试 SolrClient 使用（sql 动态拼接）
 */
@SpringBootTest
class SolrQueryApplicationTests {

    @Value("${solr.core.name}")
    private String coreName;
    @Autowired
    private SolrClient solrClient;

    /**
     * <strong>方法描述 ：</strong> 将数据库中获取的数据，插入到solr库中<br/>
     * org.apache.http.client.NonRepeatableRequestException: Cannot retry request with a non-repeatable request entity.
     * <strong>创建时间：</strong> 2021/2/20 上午9:10<br/>
     *
     * @param []
     * @return void
     */
    @Test
    public void insertDataToSolr() {
        //1.获取数据库中的数据
        List<TextDataDO> products = new ArrayList<>();
        TextDataDO demo = new TextDataDO();
        demo.setId("ASD123456789");
        demo.setTitle("ceshi add");
        demo.setContent("三生三世三生三世三三三 dddd");
        products.add(demo);
        //2.将数据插入到solr
        //用于存放所有solr数据的集合
        List<SolrInputDocument> documents = new ArrayList<>();
        //封装Document对象
        for (TextDataDO product : products) {
            SolrInputDocument document = new SolrInputDocument();
            document.addField("id", product.getId());
            document.addField("title", product.getTitle());
            document.addField("content", product.getContent());
            //存入到集合中
            documents.add(document);
        }
        try {
//            String solrUrl = "http://admin:123456@192.168.1.22:8983/solr/sino_core";
//            HttpSolrClient solrClient1 = new HttpSolrClient.Builder(solrUrl).build();
            //提交给solr客户端
            solrClient.add(coreName, documents);
            //commit
            solrClient.commit(coreName,true,true,true);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * <strong>方法描述 ：</strong> 查询solr库<br/>
     * <strong>创建时间：</strong> 2021/2/20 上午9:14<br/>
     *
     * @param []
     * @return void
     */
    @Test
    public void testQueryFromSolr() throws IOException, SolrServerException {
        //String keyword = "t_product_name:手机"; //指定字段查询
        //复制域查询
        String keyword = "新华网";
        //创建查询对象
        SolrQuery query = new SolrQuery();
        //往对象中设置参数
        //        query.set("df", " text");//复制域查询
        query.setQuery(keyword);
        query.setStart(0);
        query.setRows(10);
        //高亮
        query.addHighlightField("title");
        query.setHighlight(true);
        query.setHighlightSimplePre("<span style='color:red'>");
        query.setHighlightSimplePost("</span>");
        //执行查询.得到响应结果
        QueryResponse response = solrClient.query(coreName, query);
        //System.out.println(response);
        //封装结果集==> List<TProductSearchDTO>
        List<TextDataDO> products = new ArrayList<>();
        SolrDocumentList results = response.getResults();
        for (SolrDocument document : results) {
            TextDataDO product = new TextDataDO();
            String id = (String) document.getFieldValue("id");
            product.setId(id);
            String title = ((List) document.getFieldValue("title")).get(0).toString();
            product.setTitle(title);
            String content = ((List) document.getFieldValue("content")).get(0).toString();
            product.setContent(content);
            products.add(product);
        }
        System.out.println(JSONObject.toJSON(products));
    }

    /**
     * <strong>方法描述 ：</strong> 查询solr库 带高亮的查询并封装<br/>
     * <strong>创建时间：</strong> 2021/2/20 上午9:19<br/>
     *
     * @param []
     * @return void
     */
    @Test
    public void testQueryFromSolr1() throws IOException, SolrServerException {
        //String keyword = "t_product_name:手机"; //指定字段查询
        //复制域查询
        String keyword = "手机";
        //创建查询对象
        SolrQuery query = new SolrQuery();
        //往对象中设置参数
        //        query.set("df", "keywords");//复制域查询
        query.setQuery(keyword);
        query.setStart(0);
        query.setRows(10);
        //高亮
        query.addHighlightField("title");
        query.setHighlight(true);
        query.setHighlightSimplePre("<span style='color:red'>");
        query.setHighlightSimplePost("</span>");
        //执行查询.得到响应结果
        QueryResponse response = solrClient.query(coreName, query);
        //System.out.println(response);
        //封装结果集==> List<TProductSearchDTO>
        List<TextDataDO> products = new ArrayList<>();
        //获得数据结果集
        SolrDocumentList results = response.getResults();
        //获得高亮结果集
        Map<String, Map<String, List<String>>> highlighting = response.getHighlighting();
        for (SolrDocument document : results) {
            TextDataDO product = new TextDataDO();
            String id = (String) document.getFieldValue("id");
            product.setId(id);


            //==========从高亮结果集中那带高亮效果的title=============

            Map<String, List<String>> stringListMap = highlighting.get(id);
            List<String> title1 = stringListMap.get("title");
            if (title1 != null) {
                String title = title1.get(0);
                product.setTitle(title);
            }
            String content = ((List) document.getFieldValue("content")).get(0).toString();
            product.setContent(content);
            products.add(product);
        }
        System.out.println(JSONObject.toJSON(products));
    }

    @Test
    public void contextLoads1() throws Exception {
        SolrQuery query = new SolrQuery();
        query.set("df", "text");
        query.setQuery("(text:(\"普京\",\"俄罗斯\") AND source:\"人民网\" )OR (text:(\"张汉辉\",\"疫苗\") AND source:\"新华网\")");
        // 指定偏移量，从0开始
        query.setStart(0);
        // 查询的size数量
        query.setRows(5);
        QueryResponse response = solrClient.query(coreName, query);
        //获取当前页数据
        System.out.println(response.getResults());
        //获取记录总数
        System.out.println(response.getResults().getNumFound());
    }
}
