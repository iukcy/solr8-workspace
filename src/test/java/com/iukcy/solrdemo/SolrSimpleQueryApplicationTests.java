package com.iukcy.solrdemo;

import com.alibaba.fastjson.JSONObject;
import com.iukcy.solrdemo.model.TextDataDO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.RequestMethod;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.FilterQuery;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.data.solr.core.query.result.SolrResultPage;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @project: solr-workspace
 * @author: zhangzhufu
 * @datetime: 2021/1/25 下午12:16
 * @description: 测试SolrTemplate使用
 */
@SpringBootTest
class SolrSimpleQueryApplicationTests {

    @Value("${solr.core.name}")
    private String coreName;
    @Autowired
    private SolrTemplate solrTemplate;

    /**
     * <strong>方法描述 ：</strong> 根据主键查询记录<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:46<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads0() {
        TextDataDO count = solrTemplate.getById(coreName, "6873023038586c67e6403bd3ca7199ee", TextDataDO.class).get();
        System.out.println(count.getTitle());
    }

    @Test
    void contextLoads1() {
        TextDataDO count = solrTemplate.getById(coreName, "6873023038586c67e6403bd3ca7199ee", TextDataDO.class).get();
        System.out.println(count.getTitle());
    }

    @Test
    void contextLoads2() {
        //条件查询
        SimpleQuery query = new SimpleQuery("id:6873023038586c67e6403bd3ca7199ee");
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(count.getSize());
    }

    /**
     * <strong>方法描述 ：</strong> 未配置中文分词器，默认将每个中文变成一个词<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:47<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads3() throws Exception {
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(new Criteria("title").contains("什"));
        query.addCriteria(new Criteria("name").contains("么"));
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(JSONObject.toJSON(count.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> 测试非 new 条件<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:47<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads4() throws Exception {
        //配置ik中文分词器 模糊查询text_general
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("title").is("普京_sa"));
        //        query.addCriteria(Criteria.where("name").is("学霸_sa"));
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(JSONObject.toJSON(count.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> 测试分词<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:47<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads5() throws Exception {
        //模糊查询 返回部分列
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("title").is("学霸1111打瞌睡2002"));
        query.addProjectionOnFields("id", "name");
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(JSONObject.toJSON(count.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> 双引号包裹不会被拆分<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:48<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads6() throws Exception {
        //模糊查询 返回部分列
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("title").is("\"什么思考\""));
        query.addProjectionOnFields("id", "name");
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(JSONObject.toJSON(count.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> 测试 fuzzy TODO<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:48<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads7() throws Exception {
        //模糊查询 返回部分列 部分纠错字符数量1
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("title").fuzzy("2001", 1));
        query.addProjectionOnFields("id", "name");
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(JSONObject.toJSON(count.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> 模糊查询 返回部分列  q=name:"2001++"~1&fl=id,name 测试sloppy TODO<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:48<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads8() throws Exception {
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("title").sloppy("2001 2002 2003", 4));
        query.addProjectionOnFields("id", "name");
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(JSONObject.toJSON(count.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> boost TODO<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:48<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads9() throws Exception {
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("title").boost(4));
        query.addProjectionOnFields("id", "name");
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(JSONObject.toJSON(count.getContent()));
        System.out.println(JSONObject.toJSON(count.getSize()));
    }

    /**
     * <strong>方法描述 ：</strong> expression TODO<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:49<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads10() throws Exception {
        //模糊查询 返回部分列   q=name:2002&fl=id,name
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("title").expression("2002"));
        query.addProjectionOnFields("id", "name");
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        System.out.println(JSONObject.toJSON(count.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> is 并不是完全匹配，会被分词模糊查询/测试分页相关返回数据<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:49<br/>
     *
     * @param []
     * @return void
     */
    @Test
    void contextLoads11() throws Exception {
        //配置ik中文分词器 模糊查询
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("title").is("学霸1111打瞌睡2002"));
        query.addSort(Sort.by("updateTime").descending());
        SolrResultPage count = solrTemplate.query(coreName, query, TextDataDO.class);
        long test = solrTemplate.count(coreName, query);
        System.out.println("总数：" + test);
        // 指定偏移量，从0开始
        query.setOffset(0L);
        // 查询的size数量
        query.setRows(2);
        Page<TextDataDO> page = solrTemplate.queryForPage(coreName, query, TextDataDO.class);

        System.out.println(JSONObject.toJSON(count.getSize()));
        System.out.println(JSONObject.toJSON(count.getContent()));

        System.out.println(page.getTotalElements());
        System.out.println(page.getTotalPages());
        System.out.println(JSONObject.toJSON(page.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> 测试 or and 组合链接/为保证 or，第一个条件不会被括号包裹，注意or多个子句情况<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:49<br/>
     *
     * @param []
     * @return void
     */
    @Test
    public void contextLoads12() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH::mm:ss'Z'");
        //配置ik中文分词器 模糊查询
        SimpleQuery query = new SimpleQuery("*:*");
        //用来设置关键词空格链接符 and or
        //        query.setDefaultOperator();
        String ewen[] = ("демография,урбанизация,Москва,город,эмиграция").split(",");
        String zhongwen[] = ("中俄,中苏,俄罗斯,苏联,摩尔多瓦").split(",");
        Criteria all = Criteria.where("text").isNull();
        Criteria criteria = Criteria.where("text").in(ewen).and(Criteria.where("text").in("СТРАТЕГИЯ"));
        Criteria zhcriteria = Criteria.where("text").in(zhongwen).and(Criteria.where("text").in("科学,技术,IT,科技".split(",")));

        query.addCriteria((all.or(criteria)).or(zhcriteria));
        //        query.addCriteria((Criteria.where("id").in("a0872cc5b5ca4cc25076f3d868e1bdf8")));
        //        query.addCriteria((Criteria.where("id").in("d5cfead94f5350c12c322b5b664544c1")));
        //        query.addCriteria(Criteria.where("title").in("СТРАТЕГИЯ"));

        // and
        //        query.addFilterQuery(FilterQuery.filter(Criteria.where("createdate").between("2020-01-01 12:00:00", "2022-01-01 12:00:00")));
        query.addSort(Sort.by("createdate").descending());
        // 指定偏移量，从0开始
        query.setOffset(0L);
        // 查询的size数量
        query.setRows(5);
        Page<TextDataDO> page = solrTemplate.queryForPage(coreName, query, TextDataDO.class);

        System.out.println(page.getTotalElements());
        System.out.println(page.getTotalPages());
        System.out.println(JSONObject.toJSON(page.getContent()));
    }

    /**
     * <strong>方法描述 ：</strong> 字符串拼接，实验环境不需要 T Z<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:49<br/>
     *
     * @param []
     * @return void
     */
    @Test
    public void contextLoads13() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String time = "lostTime:[" + sdf.format(new Date()) + " TO " + sdf.format(new Date()) + "]";
        System.out.println(time);
    }

    /**
     * <strong>方法描述 ：</strong> 测试时间范围查询 实验环境不需要TZ/测试 in 语句（非精确匹配），关键子查询 空格代表 AND OR 注意为大写<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:49<br/>
     *
     * @param []
     * @return void
     */
    @Test
    public void contextLoads14() {
        //配置ik中文分词器 模糊查询
        SimpleQuery query = new SimpleQuery();
        String keywords[] = {"история", "православие", "Крепостничество", "Советский союз", "князь", "император", "царь", "СССР", "Великая отечественная война", "Ленин", "Сталин"};
        query.addCriteria(Criteria.where("title").in(keywords));
        FilterQuery filterQuery = FilterQuery.filter(Criteria.where("createdate").between("1970-01-01T00:00:00Z", "2022-01-01T00:00:00Z"));
        query.addFilterQuery(filterQuery);
        query.addProjectionOnFields("id", "title", "createdate");
        query.addSort(Sort.by("createdate").descending());
        // 指定偏移量，从0开始
        query.setOffset(0L);
        // 查询的size数量
        query.setRows(5);
        Page<TextDataDO> test = solrTemplate.queryForPage(coreName, query, TextDataDO.class, RequestMethod.POST);
        test.getContent().forEach(item -> System.out.println(JSONObject.toJSON(item)));
    }

    /**
     * <strong>方法描述 ：</strong> 日期范围查询<br/>
     * <strong>创建时间：</strong> 2021/2/19 下午3:49<br/>
     *
     * @param []
     * @return void
     */
    @Test
    public void contextLoads15() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH::mm:ss'Z'");
        //配置ik中文分词器 模糊查询
        SimpleQuery query = new SimpleQuery();
        query.addCriteria(Criteria.where("date").between("2021-01-18 01:47:00", "2021-01-18 01:47:01"));
        query.addSort(Sort.by("createdate").descending());
        // 指定偏移量，从0开始
        query.setOffset(0L);
        // 查询的size数量
        query.setRows(5);
        Page<TextDataDO> page = solrTemplate.queryForPage(coreName, query, TextDataDO.class);

        System.out.println(page.getTotalElements());
        System.out.println(page.getTotalPages());
        System.out.println(JSONObject.toJSON(page.getContent()));
    }
}
