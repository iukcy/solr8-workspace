package com.iukcy.solrdemo.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.stereotype.Component;

/**
 * @project: solr-workspace
 * @author: zhangzhufu
 * @datetime: 2021/1/25 下午12:18
 * @description: TODO
 */

@Component
public class SolrSearchService {
    @Autowired
    private SolrTemplate solrTemplate;

    public void query() {
    }
}