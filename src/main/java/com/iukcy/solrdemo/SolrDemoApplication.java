package com.iukcy.solrdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class SolrDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SolrDemoApplication.class, args);
    }

}
