package com.iukcy.solrdemo.model;

import com.alibaba.fastjson.annotation.JSONField;
import org.apache.solr.client.solrj.beans.Field;

import java.io.Serializable;

/**
 * @project: solr-workspace
 * @author: zhangzhufu
 * @datetime: 2021/1/25 下午12:40
 * @description: 测试实体
 */
public class TextDataDO implements Serializable {

    private String id;
    private String title;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @Field("createdate")
    private String createDate;
    private String content;
    private String date;
    private String url;
    @Field("static")
    private String static_info;
    private String type;
    private String keywords;
    private String source;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatic_info() {
        return static_info;
    }

    public void setStatic_info(String static_info) {
        this.static_info = static_info;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}