# 2021-02-02
## 环境：solr8 + jetty (可选择环境+ solr+tomcat)

### 1.配置core
- ①需要提前在server/solr目录下创建 test_core 将找到/server/solr/configsets/_default目录下的conf文件夹，然后拷贝一份到/server/solr/test_core目录下。
- ②solradmin管理页面-core管理菜单-添加core。
参考：https://www.cnblogs.com/116970u/p/10403713.html

### 2.solr 需要配置 ①mysql数据源 ②定时同步 ③ik分词器
```
①配置mysql数据源：core下conf、data-config.xml中配置
参考：https://www.cnblogs.com/yanfeiLiu/p/9272644.html
↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
特别注意：mysql版本问题，mysql8与之前驱动不同
url：jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC
↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
②定时同步mysql数据：需要数据表中添加 updateTime 根据时间戳变化字段
参考：https://blog.csdn.net/millery22/article/details/51445964
③配置 ik 中文分词器：按照版本下载
参考：https://www.cnblogs.com/bxcsx/p/11599650.html
④配置用户登录账号密码
参考：https://blog.csdn.net/u011561335/article/details/90695860
```
### 3.solr 使用mysql视图(多个表主键重复，solr最终视图数量并不等于各表加和:使用MD5（url）作为主键)。
>参考：数据库视图v_textdata

### 4.mysql数据库表名避免关键字，各采集网站注意使用mysql相同字段和字段顺序，方便使用select * 建立视图。
>参考：数据库视图v_textdata

### 5.日期格式化问题（建立视图时候使用mysql函数（正则表达式）统一格式）
>参考：数据库视图v_textdata

### 6.项目中lombook 与maven jdk 版本不兼容，原1.16.10 升级到 1.18.12 解决项目不能编译通过问题
报错没有提示
>参考：建议不使用 lombook 插件

### 7.使用solr，默认按照相关性排序  相关性最高的在最前面，如使用采集时间排序，可能关键词完全匹配，也会被排在后面或下一页

### 8.solr 域类型：普通域/复制域/动态域 
参考地址：
> https://spring.io/projects/spring-data-solr#learn   spring data solr 参考地址
> https://blog.csdn.net/jiangchao858/article/details/53859731 springboot data solr 解析器说明
> https://blog.csdn.net/qq_40885085/article/details/104103614 solr 域说明

### 使用方法：
>启动 solr start
>
>停止 solr stop -all

 **_程序包地址 
链接：https://pan.baidu.com/s/1Sw2pMqGfbS-NrXVoTA7wPg 
提取码：h8ja 
复制这段内容后打开百度网盘手机App，操作更方便哦_** 